package com.sohanroni.graph;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sohanroni.graph.female.FemaleWeight24GraphActivity;
import com.sohanroni.graph.female.FemaleWeight60GraphActivity;
import com.sohanroni.graph.male.ChildWeight24GraphActivity;
import com.sohanroni.graph.male.ChildWeight60GraphActivity;
import com.sohanroni.graph.male.MainActivity;

public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
    }

    public void maleHeight24(View view) {
        startActivity(new Intent(LauncherActivity.this, MainActivity.class));
    }

    public void maleWeight24(View view) {
        startActivity(new Intent(LauncherActivity.this, ChildWeight24GraphActivity.class));
    }

    public void maleWeight60(View view) {
        startActivity(new Intent(LauncherActivity.this, ChildWeight60GraphActivity.class));
    }

    public void femaleWeight24(View view) {
        startActivity(new Intent(LauncherActivity.this, FemaleWeight24GraphActivity.class));
    }

    public void femaleWeight60(View view) {
        startActivity(new Intent(LauncherActivity.this, FemaleWeight60GraphActivity.class));
    }
}
