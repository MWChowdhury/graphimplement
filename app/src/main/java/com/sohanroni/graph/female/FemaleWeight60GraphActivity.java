package com.sohanroni.graph.female;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.sohanroni.graph.R;
import com.sohanroni.plotter.female.FemaleWeight60MonthsPlotter;

import java.util.ArrayList;
import java.util.List;

public class FemaleWeight60GraphActivity extends AppCompatActivity {


    List<Entry> HeightEntries = new ArrayList<Entry>();
    LineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_female_weight_60_graph);

        chart = findViewById(R.id.chart);

        //(age,height)
        HeightEntries.add(new Entry(1, 44));
        HeightEntries.add(new Entry(2, 53));
        HeightEntries.add(new Entry(3, 57));
        HeightEntries.add(new Entry(4, 61));
        HeightEntries.add(new Entry(5, 66));
        HeightEntries.add(new Entry(6, 68));

        new FemaleWeight60MonthsPlotter().FemaleWeight60MonthsPlotter(chart, HeightEntries);
    }
}