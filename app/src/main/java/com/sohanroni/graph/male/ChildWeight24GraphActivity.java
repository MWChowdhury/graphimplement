package com.sohanroni.graph.male;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.sohanroni.graph.R;
import com.sohanroni.plotter.male.ChildWeight24MonthsPlotter;

import java.util.ArrayList;
import java.util.List;

public class ChildWeight24GraphActivity extends AppCompatActivity {


    List<Entry> HeightEntries = new ArrayList<Entry>();
    LineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_height_graph);

        chart = (LineChart) findViewById(R.id.chart);

        //(age,height)
        HeightEntries.add(new Entry(1,44 ));
        HeightEntries.add(new Entry(2,53 ));
        HeightEntries.add(new Entry(3,57 ));
        HeightEntries.add(new Entry(4,61 ));
        HeightEntries.add(new Entry(5,66 ));
        HeightEntries.add(new Entry(6,68 ));

        new ChildWeight24MonthsPlotter().ChildHeightGraphPlot(chart, HeightEntries);
    }

}
