package com.sohanroni.plotter.female;

import android.graphics.Color;
import android.os.Build;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

public class FemaleWeight24MonthsPlotter {
    LineChart chart;

    private List<Entry> Orange1 = new ArrayList<Entry>();
    private List<Entry> yollow1 = new ArrayList<Entry>();
    private List<Entry> yollow2 = new ArrayList<Entry>();
    private List<Entry> green1 = new ArrayList<Entry>();
    private List<Entry> green2 = new ArrayList<Entry>();
    private List<Entry> green3 = new ArrayList<Entry>();
    private List<Entry> white1 = new ArrayList<Entry>();
    private List<Entry> white2 = new ArrayList<Entry>();
    private List<Entry> myentrie = new ArrayList<Entry>();
    private LineDataSet mydataset;
    private ArrayList<ILineDataSet> LstDataSet = new ArrayList<ILineDataSet>();

    public FemaleWeight24MonthsPlotter() {
    }

    public void FemaleWeight24MonthsPlotter(LineChart chart, List<Entry> entry) {
        this.chart = chart;
        this.myentrie = entry;
        DrawGraph();
    }


    private void DrawGraph() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            chart.setHardwareAccelerationEnabled(false);
        }

        chart.setKeepPositionOnRotation(true);
        chart.setAutoScaleMinMaxEnabled(true);
        chart.setPinchZoom(false);
        chart.setScaleEnabled(false);
        chart.fitScreen();

        XAxis xAxis = chart.getXAxis();
        xAxis.setLabelCount(24);
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);

        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisRight().setDrawGridLines(false);


        orange1Coordinates();

        yellow2Coordinates();

        green3Coordinates();

        yellow1Coordinates();

        green1Coordinates();

        green2Coordinates();

        white1Coordinates();


        white2Coordinates();


        LineDataSet dataSetYellow2 = new LineDataSet(yollow1, "yollow"); // add Orange1 to dataSetOrange1
        dataSetYellow2.setDrawFilled(true);
        dataSetYellow2.setCircleRadius(0);
        dataSetYellow2.setDrawCircles(false);
        dataSetYellow2.setFillAlpha(255);
        dataSetYellow2.setFillAlpha(255);
        //Drawable drawablea = ContextCompat.getDrawable(mContext, R.//Drawable.filldrawdeep);
        dataSetYellow2.setFillColor(Color.parseColor("#CCCC00"));
        dataSetYellow2.setValueTextColor(Color.GREEN);
        dataSetYellow2.setDrawValues(false);
        dataSetYellow2.setLineWidth((float) 0.5);
        dataSetYellow2.setColor(Color.BLACK);


        LineDataSet dataSetgreen1 = new LineDataSet(green1, "green"); // add Orange1 to dataSetOrange1
        dataSetgreen1.setDrawFilled(true);
        dataSetgreen1.setCircleRadius(0);
        dataSetgreen1.setDrawCircles(false);
        dataSetgreen1.setFillAlpha(255);
        //Drawable drawablesb = ContextCompat.getDrawable(mContext, R.//Drawable.filldrawm);
        dataSetgreen1.setFillColor(Color.parseColor("#008000"));
        dataSetgreen1.setValueTextColor(Color.GREEN);
        dataSetgreen1.setDrawValues(false);
        dataSetgreen1.setLineWidth((float) 0.5);
        dataSetgreen1.setColor(Color.BLACK);


        LineDataSet dataSetgreen2 = new LineDataSet(green2, "greeen"); // add Orange1 to dataSetOrange1
        dataSetgreen2.setDrawFilled(true);
        dataSetgreen2.setCircleRadius(0);
        dataSetgreen2.setDrawCircles(false);
        dataSetgreen2.setFillAlpha(255);
        dataSetgreen2.setFillColor(Color.parseColor("#008000"));
        dataSetgreen2.setValueTextColor(Color.GREEN);
        dataSetgreen2.setDrawValues(false);
        dataSetgreen2.setLineWidth((float) 0.5);
        dataSetgreen2.setColor(Color.BLACK);


        LineDataSet dataSetwhite1 = new LineDataSet(white1, "white"); // add Orange1 to dataSetOrange1
        dataSetwhite1.setDrawFilled(false);
        dataSetwhite1.setCircleRadius(0);
        dataSetwhite1.setDrawCircles(false);
        dataSetwhite1.setFillAlpha(255);
        dataSetwhite1.setValueTextColor(Color.GREEN);
        dataSetwhite1.setDrawValues(false);
        dataSetwhite1.setLineWidth((float) 0.5);
        dataSetwhite1.setColor(Color.BLACK);


        LineDataSet dataSetOrange1 = new LineDataSet(Orange1, "orange"); // add Orange1 to dataSetOrange1
        dataSetOrange1.setDrawFilled(true);
        dataSetOrange1.setDrawCircles(false);
        dataSetOrange1.setCircleRadius(0);
        dataSetOrange1.setFillAlpha(255);
        dataSetOrange1.setDrawValues(false);
        dataSetOrange1.setFillColor(Color.parseColor("#FFA500"));
        dataSetOrange1.setValueTextColor(Color.RED);
        dataSetOrange1.setLineWidth((float) 0.5);
        dataSetOrange1.setColor(Color.BLACK);

        LineDataSet dataSetyollow2 = new LineDataSet(yollow2, "yellow"); // add Orange1 to dataSetOrange1
        dataSetyollow2.setDrawFilled(true);
        dataSetyollow2.setCircleRadius(0);
        dataSetyollow2.setDrawCircles(false);
        dataSetyollow2.setFillAlpha(255);
        //dataSetyollow2.setFillDrawable(drawables);
        dataSetyollow2.setFillColor(Color.parseColor("#FFFF00"));
        dataSetyollow2.setValueTextColor(Color.GREEN);
        dataSetyollow2.setDrawValues(false);
        dataSetyollow2.setLineWidth((float) 0.5);
        dataSetyollow2.setColor(Color.BLACK);


        LineDataSet dataSetgreen3 = new LineDataSet(green3, "point"); // add Orange1 to dataSetOrange1
        dataSetgreen3.setDrawFilled(true);
        dataSetgreen3.setCircleRadius(0);
        dataSetgreen3.setFillColor(Color.parseColor("#008000"));
        dataSetgreen3.setDrawCircles(false);
        dataSetgreen3.setCircleColor(Color.RED);
        dataSetgreen3.setDrawValues(false);
        dataSetgreen3.setValueTextColor(Color.BLACK);
        dataSetgreen3.setLineWidth((float) 0.5);
        dataSetgreen3.setColor(Color.BLACK);


        LineDataSet dataSetwhite2 = new LineDataSet(white2, "white"); // add Orange1 to dataSetOrange1
        dataSetwhite2.setDrawFilled(false);
        dataSetwhite2.setCircleRadius(0);
        dataSetwhite2.setDrawCircles(false);
        dataSetwhite2.setFillAlpha(255);
        dataSetwhite2.setValueTextColor(Color.GREEN);
        dataSetwhite2.setDrawValues(false);
        //  dataSetwhite2.setLineWidth((float) 0.5);
        //  dataSetwhite2.setColor(Color.BLACK);


        LstDataSet.add(dataSetwhite1);
        LstDataSet.add(dataSetgreen3);
        LstDataSet.add(dataSetgreen2);
        LstDataSet.add(dataSetgreen1);
        LstDataSet.add(dataSetyollow2);
        LstDataSet.add(dataSetYellow2);
        LstDataSet.add(dataSetOrange1);
        LstDataSet.add(dataSetwhite2);






       /* if(myentrie.size() >0) {
            Log.v("rabby","list Size:"+myentrie.size());
            mydataset = new LineDataSet(myentrie, "age");
            // add Orange1 to dataSetOrange1
            mydataset.setLineWidth(3);
            mydataset.setColor(Color.WHITE);
            mydataset.setDrawFilled(false);
            mydataset.setDrawCircles(true);
            mydataset.setCircleRadius(2);
            mydataset.setCircleColor(Color.BLUE);
            mydataset.setDrawValues(false);
            LstDataSet.add(mydataset);
        }*/

        LineData data = new LineData(LstDataSet);
        chart.setData(data);
        chart.getData().setHighlightEnabled(false);
        chart.invalidate();
    }

    private void white2Coordinates() {
        white2.add(new Entry(0, (float) 0));
        white2.add(new Entry(1, (float) 1.5));
        white2.add(new Entry(2, (float) 2.1));
        white2.add(new Entry(3, (float) 2.6));
        white2.add(new Entry(4, (float) 3.0));
        white2.add(new Entry(5, (float) 3.3));
        white2.add(new Entry(6, (float) 3.6));
        white2.add(new Entry(7, (float) 3.9));
        white2.add(new Entry(8, (float) 4.1));
        white2.add(new Entry(9, (float) 4.3));
        white2.add(new Entry(10, (float) 4.5));
        white2.add(new Entry(11, (float) 4.7));
        white2.add(new Entry(12, (float) 4.9));
        white2.add(new Entry(13, (float) 5.0));
        white2.add(new Entry(14, (float) 5.1));
        white2.add(new Entry(15, (float) 5.2));
        white2.add(new Entry(16, (float) 5.3));
        white2.add(new Entry(17, (float) 5.4));
        white2.add(new Entry(18, (float) 5.5));
        white2.add(new Entry(19, (float) 5.6));
        white2.add(new Entry(20, (float) 5.7));
        white2.add(new Entry(21, (float) 5.8));
        white2.add(new Entry(22, (float) 5.9));
        white2.add(new Entry(23, (float) 5.99));
        white2.add(new Entry(24, (float) 6.0));
    }

    private void white1Coordinates() {
        white1.add(new Entry(0, (float) 4.8));
        white1.add(new Entry(1, (float) 6.15));
        white1.add(new Entry(2, (float) 7.5));
        white1.add(new Entry(3, (float) 8.5));
        white1.add(new Entry(4, (float) 9.45));
        white1.add(new Entry(5, (float) 10.0));
        white1.add(new Entry(6, (float) 10.6));
        white1.add(new Entry(7, (float) 11));
        white1.add(new Entry(8, (float) 11.6));
        white1.add(new Entry(9, (float) 12.0));
        white1.add(new Entry(10, (float) 12.4));
        white1.add(new Entry(11, (float) 12.7));
        white1.add(new Entry(12, (float) 13.1));
        white1.add(new Entry(13, (float) 13.5));
        white1.add(new Entry(14, (float) 13.7));
        white1.add(new Entry(15, (float) 14.1));
        white1.add(new Entry(16, (float) 14.5));
        white1.add(new Entry(17, (float) 14.8));
        white1.add(new Entry(18, (float) 15.1));
        white1.add(new Entry(19, (float) 15.5));
        white1.add(new Entry(20, (float) 15.7));
        white1.add(new Entry(21, (float) 16.0));
        white1.add(new Entry(22, (float) 16.4));
        white1.add(new Entry(23, (float) 16.7));
        white1.add(new Entry(24, (float) 17.0));
    }

    private void green2Coordinates() {
        green2.add(new Entry(0, (float) 3.7));
        green2.add(new Entry(1, (float) 4.7));
        green2.add(new Entry(2, (float) 5.8));
        green2.add(new Entry(3, (float) 6.6));
        green2.add(new Entry(4, (float) 7.4));
        green2.add(new Entry(5, (float) 7.8));
        green2.add(new Entry(6, (float) 8.2));
        green2.add(new Entry(7, (float) 8.6));
        green2.add(new Entry(8, (float) 9.0));
        green2.add(new Entry(9, (float) 9.3));
        green2.add(new Entry(10, (float) 9.6));
        green2.add(new Entry(11, (float) 9.9));
        green2.add(new Entry(12, (float) 10.1));
        green2.add(new Entry(13, (float) 10.4));
        green2.add(new Entry(14, (float) 10.6));
        green2.add(new Entry(15, (float) 10.9));
        green2.add(new Entry(16, (float) 11.1));
        green2.add(new Entry(17, (float) 11.4));
        green2.add(new Entry(18, (float) 11.6));
        green2.add(new Entry(19, (float) 11.7));
        green2.add(new Entry(20, (float) 12.1));
        green2.add(new Entry(21, (float) 12.3));
        green2.add(new Entry(22, (float) 12.5));
        green2.add(new Entry(23, (float) 12.8));
        green2.add(new Entry(24, (float) 13.0));
    }

    private void green1Coordinates() {
        green1.add(new Entry(0, (float) 3.2));
        green1.add(new Entry(1, (float) 4.2));
        green1.add(new Entry(2, (float) 5.05));
        green1.add(new Entry(3, (float) 5.8));
        green1.add(new Entry(4, (float) 6.4));
        green1.add(new Entry(5, (float) 6.8));
        green1.add(new Entry(6, (float) 7.3));
        green1.add(new Entry(7, (float) 7.6));
        green1.add(new Entry(8, (float) 7.9));
        green1.add(new Entry(9, (float) 8.2));
        green1.add(new Entry(10, (float) 8.5));
        green1.add(new Entry(11, (float) 8.7));
        green1.add(new Entry(12, (float) 8.9));
        green1.add(new Entry(13, (float) 9.25));
        green1.add(new Entry(14, (float) 9.4));
        green1.add(new Entry(15, (float) 9.6));
        green1.add(new Entry(16, (float) 9.8));
        green1.add(new Entry(17, (float) 10.0));
        green1.add(new Entry(18, (float) 10.2));
        green1.add(new Entry(19, (float) 10.45));
        green1.add(new Entry(20, (float) 10.65));
        green1.add(new Entry(21, (float) 10.9));
        green1.add(new Entry(22, (float) 11.05));
        green1.add(new Entry(23, (float) 11.4));
        green1.add(new Entry(24, (float) 11.5));
    }

    private void green3Coordinates() {
        green3.add(new Entry(0, (float) 4.2));
        green3.add(new Entry(1, (float) 5.5));
        green3.add(new Entry(2, (float) 6.65));
        green3.add(new Entry(3, (float) 7.5));
        green3.add(new Entry(4, (float) 8.35));
        green3.add(new Entry(5, (float) 8.75));
        green3.add(new Entry(6, (float) 9.4));
        green3.add(new Entry(7, (float) 9.7));
        green3.add(new Entry(8, (float) 10.2));
        green3.add(new Entry(9, (float) 10.5));
        green3.add(new Entry(10, (float) 10.85));
        green3.add(new Entry(11, (float) 11.25));
        green3.add(new Entry(12, (float) 11.5));
        green3.add(new Entry(13, (float) 11.8));
        green3.add(new Entry(14, (float) 12.1));
        green3.add(new Entry(15, (float) 12.45));
        green3.add(new Entry(16, (float) 12.55));
        green3.add(new Entry(17, (float) 12.85));
        green3.add(new Entry(18, (float) 13.25));
        green3.add(new Entry(19, (float) 13.5));
        green3.add(new Entry(20, (float) 13.7));
        green3.add(new Entry(21, (float) 14.0));
        green3.add(new Entry(22, (float) 14.35));
        green3.add(new Entry(23, (float) 14.6));
        green3.add(new Entry(24, (float) 14.75));
    }

    private void yellow2Coordinates() {
        yollow2.add(new Entry(0, (float) 2.85));
        yollow2.add(new Entry(1, (float) 3.65));
        yollow2.add(new Entry(2, (float) 4.50));
        yollow2.add(new Entry(3, (float) 5.25));
        yollow2.add(new Entry(4, (float) 5.7));
        yollow2.add(new Entry(5, (float) 6.1));
        yollow2.add(new Entry(6, (float) 6.5));
        yollow2.add(new Entry(7, (float) 6.8));
        yollow2.add(new Entry(8, (float) 7.0));
        yollow2.add(new Entry(9, (float) 7.3));
        yollow2.add(new Entry(10, (float) 7.5));
        yollow2.add(new Entry(11, (float) 7.7));
        yollow2.add(new Entry(12, (float) 7.9));
        yollow2.add(new Entry(13, (float) 8.1));
        yollow2.add(new Entry(14, (float) 8.3));
        yollow2.add(new Entry(15, (float) 8.5));
        yollow2.add(new Entry(16, (float) 8.7));
        yollow2.add(new Entry(17, (float) 8.9));
        yollow2.add(new Entry(18, (float) 9.1));
        yollow2.add(new Entry(19, (float) 9.2));
        yollow2.add(new Entry(20, (float) 9.4));
        yollow2.add(new Entry(21, (float) 9.6));
        yollow2.add(new Entry(22, (float) 9.8));
        yollow2.add(new Entry(23, (float) 10.0));
        yollow2.add(new Entry(24, (float) 10.2));
    }

    private void yellow1Coordinates() {
        yollow1.add(new Entry(0, (float) 2.5));
        yollow1.add(new Entry(1, (float) 3.25));
        yollow1.add(new Entry(2, (float) 3.9));
        yollow1.add(new Entry(3, (float) 4.5));
        yollow1.add(new Entry(4, (float) 5.0));
        yollow1.add(new Entry(5, (float) 5.4));
        yollow1.add(new Entry(6, (float) 5.7));
        yollow1.add(new Entry(7, (float) 6.0));
        yollow1.add(new Entry(8, (float) 6.3));
        yollow1.add(new Entry(9, (float) 6.5));
        yollow1.add(new Entry(10, (float) 6.7));
        yollow1.add(new Entry(11, (float) 6.9));
        yollow1.add(new Entry(12, (float) 7.0));
        yollow1.add(new Entry(13, (float) 7.2));
        yollow1.add(new Entry(14, (float) 7.4));
        yollow1.add(new Entry(15, (float) 7.6));
        yollow1.add(new Entry(16, (float) 7.7));
        yollow1.add(new Entry(17, (float) 7.9));
        yollow1.add(new Entry(18, (float) 8.0));
        yollow1.add(new Entry(19, (float) 8.2));
        yollow1.add(new Entry(20, (float) 8.4));
        yollow1.add(new Entry(21, (float) 8.6));
        yollow1.add(new Entry(22, (float) 8.7));
        yollow1.add(new Entry(23, (float) 8.9));
        yollow1.add(new Entry(24, (float) 9.0));
    }

    private void orange1Coordinates() {
        Orange1.add(new Entry(0, (float) 2.0));
        Orange1.add(new Entry(1, (float) 2.7));
        Orange1.add(new Entry(2, (float) 3.4));
        Orange1.add(new Entry(3, (float) 4.0));
        Orange1.add(new Entry(4, (float) 4.4));
        Orange1.add(new Entry(5, (float) 4.7));
        Orange1.add(new Entry(6, (float) 5.1));
        Orange1.add(new Entry(7, (float) 5.3));
        Orange1.add(new Entry(8, (float) 5.6));
        Orange1.add(new Entry(9, (float) 5.8));
        Orange1.add(new Entry(10, (float) 5.9));
        Orange1.add(new Entry(11, (float) 6.1));
        Orange1.add(new Entry(12, (float) 6.3));
        Orange1.add(new Entry(13, (float) 6.4));
        Orange1.add(new Entry(14, (float) 6.6));
        Orange1.add(new Entry(15, (float) 6.7));
        Orange1.add(new Entry(16, (float) 6.9));
        Orange1.add(new Entry(17, (float) 7.0));
        Orange1.add(new Entry(18, (float) 7.2));
        Orange1.add(new Entry(19, (float) 7.3));
        Orange1.add(new Entry(20, (float) 7.5));
        Orange1.add(new Entry(21, (float) 7.6));
        Orange1.add(new Entry(22, (float) 7.8));
        Orange1.add(new Entry(23, (float) 7.9));
        Orange1.add(new Entry(24, (float) 8.1));
    }
}