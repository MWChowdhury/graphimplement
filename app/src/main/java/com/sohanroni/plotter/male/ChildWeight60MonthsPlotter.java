package com.sohanroni.plotter.male;

import android.graphics.Color;
import android.os.Build;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

public class ChildWeight60MonthsPlotter {
    LineChart chart;

    private List<Entry> Orange1 = new ArrayList<Entry>();
    private List<Entry> yollow1 = new ArrayList<Entry>();
    private List<Entry> yollow2 = new ArrayList<Entry>();
    private List<Entry> green1 = new ArrayList<Entry>();
    private List<Entry> green2 = new ArrayList<Entry>();
    private List<Entry> green3 = new ArrayList<Entry>();
    private List<Entry> white1 = new ArrayList<Entry>();
    private List<Entry> white2 = new ArrayList<Entry>();
    private List<Entry> myentrie = new ArrayList<Entry>();
    private LineDataSet mydataset;
    private ArrayList<ILineDataSet> LstDataSet = new ArrayList<ILineDataSet>();

    public ChildWeight60MonthsPlotter() {
    }

    public void FullHeightPlotter(LineChart chart, List<Entry> entry) {
        this.chart = chart;
        this.myentrie = entry;
        DrawGraph();
    }


    private void DrawGraph() {

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            chart.setHardwareAccelerationEnabled(false);
        }

        chart.setKeepPositionOnRotation(true);
        chart.setAutoScaleMinMaxEnabled(true);
        chart.setPinchZoom(false);
        chart.setScaleEnabled(false);
        chart.fitScreen();


        XAxis xAxis = chart.getXAxis();
        //xAxis.setLabelCount(24);
        xAxis.setAxisMinimum(24);
        xAxis.setAxisMaximum(60);
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);


        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisRight().setDrawGridLines(false);


        xyValue();


        initPointsOnScreen();
    }

    private void initPointsOnScreen() {
        LineDataSet dataSetYellow2 = new LineDataSet(yollow1, "yollow"); // add Orange1 to dataSetOrange1
        dataSetYellow2.setDrawFilled(true);
        dataSetYellow2.setCircleRadius(0);
        dataSetYellow2.setDrawCircles(false);
        dataSetYellow2.setFillAlpha(255);
        dataSetYellow2.setFillAlpha(255);
        //Drawable drawablea = ContextCompat.getDrawable(mContext, R.//Drawable.filldrawdeep);
        dataSetYellow2.setFillColor(Color.parseColor("#EBCF00"));
        dataSetYellow2.setValueTextColor(Color.GREEN);
        dataSetYellow2.setDrawValues(false);
        dataSetYellow2.setLineWidth((float) 0.5);
        dataSetYellow2.setColor(Color.BLACK);


        LineDataSet dataSetgreen1 = new LineDataSet(green1, "green"); // add Orange1 to dataSetOrange1
        dataSetgreen1.setDrawFilled(true);
        dataSetgreen1.setCircleRadius(0);
        dataSetgreen1.setDrawCircles(false);
        dataSetgreen1.setFillAlpha(255);
        //Drawable drawablesb = ContextCompat.getDrawable(mContext, R.//Drawable.filldrawm);
        dataSetgreen1.setFillColor(Color.parseColor("#00C00C"));
        dataSetgreen1.setValueTextColor(Color.GREEN);
        dataSetgreen1.setDrawValues(false);
        dataSetgreen1.setLineWidth((float) 0.5);
        dataSetgreen1.setColor(Color.BLACK);


        LineDataSet dataSetgreen2 = new LineDataSet(green2, "greeen"); // add Orange1 to dataSetOrange1
        dataSetgreen2.setDrawFilled(true);
        dataSetgreen2.setCircleRadius(0);
        dataSetgreen2.setDrawCircles(false);
        dataSetgreen2.setFillAlpha(255);
        dataSetgreen2.setFillColor(Color.parseColor("#00C00C"));
        dataSetgreen2.setValueTextColor(Color.GREEN);
        dataSetgreen2.setDrawValues(false);
        dataSetgreen2.setLineWidth((float) 0.5);
        dataSetgreen2.setColor(Color.BLACK);


        LineDataSet dataSetwhite1 = new LineDataSet(white1, "white"); // add Orange1 to dataSetOrange1
        dataSetwhite1.setDrawFilled(false);
        dataSetwhite1.setCircleRadius(0);
        dataSetwhite1.setDrawCircles(false);
        dataSetwhite1.setFillAlpha(255);
        dataSetwhite1.setValueTextColor(Color.GREEN);
        dataSetwhite1.setDrawValues(false);
        dataSetwhite1.setLineWidth((float) 0.5);
        dataSetwhite1.setColor(Color.BLACK);


        LineDataSet dataSetOrange1 = new LineDataSet(Orange1, "orange"); // add Orange1 to dataSetOrange1
        dataSetOrange1.setDrawFilled(true);
        dataSetOrange1.setDrawCircles(false);
        dataSetOrange1.setCircleRadius(0);
        dataSetOrange1.setFillAlpha(255);
        dataSetOrange1.setDrawValues(false);
        dataSetOrange1.setFillColor(Color.parseColor("#ff8300"));
        dataSetOrange1.setValueTextColor(Color.RED);
        dataSetOrange1.setLineWidth((float) 0.5);
        dataSetOrange1.setColor(Color.BLACK);

        LineDataSet dataSetyollow2 = new LineDataSet(yollow2, "yellow"); // add Orange1 to dataSetOrange1
        dataSetyollow2.setDrawFilled(true);
        dataSetyollow2.setCircleRadius(0);
        dataSetyollow2.setDrawCircles(false);
        dataSetyollow2.setFillAlpha(255);
        //dataSetyollow2.setFillDrawable(drawables);
        dataSetyollow2.setFillColor(Color.parseColor("#EDF026"));
        dataSetyollow2.setValueTextColor(Color.GREEN);
        dataSetyollow2.setDrawValues(false);
        dataSetyollow2.setLineWidth((float) 0.5);
        dataSetyollow2.setColor(Color.BLACK);


        LineDataSet dataSetgreen3 = new LineDataSet(green3, "point"); // add Orange1 to dataSetOrange1
        dataSetgreen3.setDrawFilled(true);
        dataSetgreen3.setCircleRadius(0);
        dataSetgreen3.setFillColor(Color.parseColor("#00C00C"));
        dataSetgreen3.setDrawCircles(false);
        dataSetgreen3.setCircleColor(Color.RED);
        dataSetgreen3.setDrawValues(false);
        dataSetgreen3.setValueTextColor(Color.BLACK);
        dataSetgreen3.setLineWidth((float) 0.5);
        dataSetgreen3.setColor(Color.BLACK);


        LineDataSet dataSetwhite2 = new LineDataSet(white2, "white"); // add Orange1 to dataSetOrange1
        dataSetwhite2.setDrawFilled(false);
        dataSetwhite2.setCircleRadius(0);
        dataSetwhite2.setDrawCircles(false);
        dataSetwhite2.setFillAlpha(255);
        dataSetwhite2.setValueTextColor(Color.GREEN);
        dataSetwhite2.setDrawValues(false);
        //  dataSetwhite2.setLineWidth((float) 0.5);
        //  dataSetwhite2.setColor(Color.BLACK);


        LstDataSet.add(dataSetwhite1);
        LstDataSet.add(dataSetgreen3);
        LstDataSet.add(dataSetgreen2);
        LstDataSet.add(dataSetgreen1);
        LstDataSet.add(dataSetyollow2);
        LstDataSet.add(dataSetYellow2);
        LstDataSet.add(dataSetOrange1);
        LstDataSet.add(dataSetwhite2);


        LineData data = new LineData(LstDataSet);
        chart.setData(data);
        chart.getData().setHighlightEnabled(false);
        chart.invalidate();
    }

    private void xyValue() {
        Orange1.add(new Entry(24, (float) 8.6));
        Orange1.add(new Entry(25, (float) 8.8));
        Orange1.add(new Entry(26, (float) 8.9));
        Orange1.add(new Entry(27, (float) 9));
        Orange1.add(new Entry(28, (float) 9.1));
        Orange1.add(new Entry(29, (float) 9.2));
        Orange1.add(new Entry(30, (float) 9.4));
        Orange1.add(new Entry(31, (float) 9.5));
        Orange1.add(new Entry(32, (float) 9.6));
        Orange1.add(new Entry(33, (float) 9.7));
        Orange1.add(new Entry(34, (float) 9.8));
        Orange1.add(new Entry(35, (float) 9.9));
        Orange1.add(new Entry(36, (float) 10));
        Orange1.add(new Entry(37, (float) 10.1));
        Orange1.add(new Entry(38, (float) 10.2));
        Orange1.add(new Entry(39, (float) 10.3));
        Orange1.add(new Entry(40, (float) 10.4));
        Orange1.add(new Entry(41, (float) 10.5));
        Orange1.add(new Entry(42, (float) 10.6));
        Orange1.add(new Entry(43, (float) 10.7));
        Orange1.add(new Entry(44, (float) 10.8));
        Orange1.add(new Entry(45, (float) 10.9));
        Orange1.add(new Entry(46, (float) 11.0));
        Orange1.add(new Entry(47, (float) 11.1));
        Orange1.add(new Entry(48, (float) 11.2));
        Orange1.add(new Entry(49, (float) 11.3));
        Orange1.add(new Entry(50, (float) 11.4));
        Orange1.add(new Entry(51, (float) 11.5));
        Orange1.add(new Entry(52, (float) 11.6));
        Orange1.add(new Entry(53, (float) 11.7));
        Orange1.add(new Entry(54, (float) 11.8));
        Orange1.add(new Entry(55, (float) 11.9));
        Orange1.add(new Entry(56, (float) 12.0));
        Orange1.add(new Entry(57, (float) 12.1));
        Orange1.add(new Entry(58, (float) 12.2));
        Orange1.add(new Entry(59, (float) 12.3));
        Orange1.add(new Entry(60, (float) 12.4));

        yollow2.add(new Entry(24, (float) 10.8));
        yollow2.add(new Entry(25, (float) 11));
        yollow2.add(new Entry(26, (float) 11.1));
        yollow2.add(new Entry(27, (float) 11.3));
        yollow2.add(new Entry(28, (float) 11.5));
        yollow2.add(new Entry(29, (float) 11.7));
        yollow2.add(new Entry(30, (float) 11.8));
        yollow2.add(new Entry(31, (float) 12));
        yollow2.add(new Entry(32, (float) 12.1));
        yollow2.add(new Entry(33, (float) 12.3));
        yollow2.add(new Entry(34, (float) 12.4));
        yollow2.add(new Entry(35, (float) 12.55));
        yollow2.add(new Entry(36, (float) 12.7));
        yollow2.add(new Entry(37, (float) 12.9));
        yollow2.add(new Entry(38, (float) 13));
        yollow2.add(new Entry(39, (float) 13.1));
        yollow2.add(new Entry(40, (float) 13.35));
        yollow2.add(new Entry(41, (float) 13.4));
        yollow2.add(new Entry(42, (float) 13.5));
        yollow2.add(new Entry(43, (float) 13.6));
        yollow2.add(new Entry(44, (float) 13.8));
        yollow2.add(new Entry(45, (float) 14));
        yollow2.add(new Entry(46, (float) 14.1));
        yollow2.add(new Entry(47, (float) 14.35));
        yollow2.add(new Entry(48, (float) 14.4));
        yollow2.add(new Entry(49, (float) 14.5));
        yollow2.add(new Entry(50, (float) 14.7));
        yollow2.add(new Entry(51, (float) 14.8));
        yollow2.add(new Entry(52, (float) 15));
        yollow2.add(new Entry(53, (float) 15.1));
        yollow2.add(new Entry(54, (float) 15.2));
        yollow2.add(new Entry(55, (float) 15.4));
        yollow2.add(new Entry(56, (float) 15.5));
        yollow2.add(new Entry(57, (float) 15.6));
        yollow2.add(new Entry(58, (float) 15.75));
        yollow2.add(new Entry(59, (float) 15.85));
        yollow2.add(new Entry(60, (float) 16));


        /*-----------------------------------------*/
        yollow1.add(new Entry(24, (float) 9.7));
        yollow1.add(new Entry(25, (float) 9.8));
        yollow1.add(new Entry(26, (float) 10.0));
        yollow1.add(new Entry(27, (float) 10.1));
        yollow1.add(new Entry(28, (float) 10.2));
        yollow1.add(new Entry(29, (float) 10.4));
        yollow1.add(new Entry(30, (float) 10.5));
        yollow1.add(new Entry(31, (float) 10.7));
        yollow1.add(new Entry(32, (float) 10.8));
        yollow1.add(new Entry(33, (float) 10.9));
        yollow1.add(new Entry(34, (float) 11.0));
        yollow1.add(new Entry(35, (float) 11.2));
        yollow1.add(new Entry(36, (float) 11.3));
        yollow1.add(new Entry(37, (float) 11.4));
        yollow1.add(new Entry(38, (float) 11.5));
        yollow1.add(new Entry(39, (float) 11.6));
        yollow1.add(new Entry(40, (float) 11.7));
        yollow1.add(new Entry(41, (float) 11.8));
        yollow1.add(new Entry(42, (float) 11.9));
        yollow1.add(new Entry(43, (float) 12.0));
        yollow1.add(new Entry(44, (float) 12.1));
        yollow1.add(new Entry(45, (float) 12.2));
        yollow1.add(new Entry(46, (float) 12.3));
        yollow1.add(new Entry(47, (float) 12.5));
        yollow1.add(new Entry(48, (float) 12.65));
        yollow1.add(new Entry(49, (float) 12.75));
        yollow1.add(new Entry(50, (float) 13.00));
        yollow1.add(new Entry(51, (float) 13.15));
        yollow1.add(new Entry(52, (float) 13.25));
        yollow1.add(new Entry(53, (float) 13.35));
        yollow1.add(new Entry(54, (float) 13.4));
        yollow1.add(new Entry(55, (float) 13.55));
        yollow1.add(new Entry(56, (float) 13.65));
        yollow1.add(new Entry(57, (float) 13.75));
        yollow1.add(new Entry(58, (float) 13.85));
        yollow1.add(new Entry(59, (float) 13.95));
        yollow1.add(new Entry(60, (float) 14.00));


        /*-----------------------------------------------*/
        white1.add(new Entry(24, (float) 17.1));
        white1.add(new Entry(25, (float) 17.5));
        white1.add(new Entry(26, (float) 17.8));
        white1.add(new Entry(27, (float) 18.1));
        white1.add(new Entry(28, (float) 18.4));
        white1.add(new Entry(29, (float) 18.7));
        white1.add(new Entry(30, (float) 19));
        white1.add(new Entry(31, (float) 19.4));
        white1.add(new Entry(32, (float) 19.6));
        white1.add(new Entry(33, (float) 20.0));
        white1.add(new Entry(34, (float) 20.2));
        white1.add(new Entry(35, (float) 20.5));
        white1.add(new Entry(36, (float) 20.7));
        white1.add(new Entry(37, (float) 21.0));
        white1.add(new Entry(38, (float) 21.3));
        white1.add(new Entry(39, (float) 21.6));
        white1.add(new Entry(40, (float) 21.8));
        white1.add(new Entry(41, (float) 22.1));
        white1.add(new Entry(42, (float) 22.4));
        white1.add(new Entry(43, (float) 22.7));
        white1.add(new Entry(44, (float) 23.0));
        white1.add(new Entry(45, (float) 23.3));
        white1.add(new Entry(46, (float) 23.6));
        white1.add(new Entry(47, (float) 23.9));
        white1.add(new Entry(48, (float) 24.3));
        white1.add(new Entry(49, (float) 24.5));
        white1.add(new Entry(50, (float) 24.8));
        white1.add(new Entry(51, (float) 25.0));
        white1.add(new Entry(52, (float) 25.4));
        white1.add(new Entry(53, (float) 25.7));
        white1.add(new Entry(54, (float) 26.0));
        white1.add(new Entry(55, (float) 26.3));
        white1.add(new Entry(56, (float) 26.6));
        white1.add(new Entry(57, (float) 26.9));
        white1.add(new Entry(58, (float) 27.2));
        white1.add(new Entry(59, (float) 27.6));
        white1.add(new Entry(60, (float) 28.0));

        white2.add(new Entry(24, (float) 6.0));
        white2.add(new Entry(25, (float) 6.1));
        white2.add(new Entry(26, (float) 6.2));
        white2.add(new Entry(27, (float) 6.3));
        white2.add(new Entry(28, (float) 6.4));
        white2.add(new Entry(29, (float) 6.5));
        white2.add(new Entry(30, (float) 6.5));
        white2.add(new Entry(31, (float) 6.6));
        white2.add(new Entry(32, (float) 6.7));
        white2.add(new Entry(33, (float) 6.8));
        white2.add(new Entry(34, (float) 6.9));
        white2.add(new Entry(35, (float) 7.0));
        white2.add(new Entry(36, (float) 7.0));
        white2.add(new Entry(37, (float) 7.1));
        white2.add(new Entry(38, (float) 7.2));
        white2.add(new Entry(39, (float) 7.3));
        white2.add(new Entry(40, (float) 7.4));
        white2.add(new Entry(41, (float) 7.5));
        white2.add(new Entry(42, (float) 7.5));
        white2.add(new Entry(43, (float) 7.6));
        white2.add(new Entry(44, (float) 7.7));
        white2.add(new Entry(45, (float) 7.8));
        white2.add(new Entry(46, (float) 7.9));
        white2.add(new Entry(47, (float) 8.0));
        white2.add(new Entry(48, (float) 8.0));
        white2.add(new Entry(49, (float) 8.1));
        white2.add(new Entry(50, (float) 8.2));
        white2.add(new Entry(51, (float) 8.3));
        white2.add(new Entry(52, (float) 8.4));
        white2.add(new Entry(53, (float) 8.5));
        white2.add(new Entry(54, (float) 8.5));
        white2.add(new Entry(55, (float) 8.6));
        white2.add(new Entry(56, (float) 8.7));
        white2.add(new Entry(57, (float) 8.8));
        white2.add(new Entry(58, (float) 8.9));
        white2.add(new Entry(59, (float) 9));
        white2.add(new Entry(60, (float) 9));


        green1.add(new Entry(24, (float) 12.2));
        green1.add(new Entry(25, (float) 12.4));
        green1.add(new Entry(26, (float) 12.5));
        green1.add(new Entry(27, (float) 12.7));
        green1.add(new Entry(28, (float) 12.9));
        green1.add(new Entry(29, (float) 13.1));
        green1.add(new Entry(30, (float) 13.3));
        green1.add(new Entry(31, (float) 13.5));
        green1.add(new Entry(32, (float) 13.7));
        green1.add(new Entry(33, (float) 13.8));
        green1.add(new Entry(34, (float) 14.0));
        green1.add(new Entry(35, (float) 14.2));
        green1.add(new Entry(36, (float) 14.3));
        green1.add(new Entry(37, (float) 14.5));
        green1.add(new Entry(38, (float) 14.7));
        green1.add(new Entry(39, (float) 14.8));
        green1.add(new Entry(40, (float) 15));
        green1.add(new Entry(41, (float) 15.2));
        green1.add(new Entry(42, (float) 15.3));
        green1.add(new Entry(43, (float) 15.5));
        green1.add(new Entry(44, (float) 15.7));
        green1.add(new Entry(45, (float) 15.8));
        green1.add(new Entry(46, (float) 16));
        green1.add(new Entry(47, (float) 16.2));
        green1.add(new Entry(48, (float) 16.3));
        green1.add(new Entry(49, (float) 16.5));
        green1.add(new Entry(50, (float) 16.7));
        green1.add(new Entry(51, (float) 16.8));
        green1.add(new Entry(52, (float) 17.0));
        green1.add(new Entry(53, (float) 17.2));
        green1.add(new Entry(54, (float) 17.3));
        green1.add(new Entry(55, (float) 17.3));
        green1.add(new Entry(56, (float) 17.5));
        green1.add(new Entry(57, (float) 17.7));
        green1.add(new Entry(58, (float) 18.0));
        green1.add(new Entry(59, (float) 18.2));
        green1.add(new Entry(60, (float) 18.3));


        green2.add(new Entry(24, (float) 13.6));
        green2.add(new Entry(25, (float) 13.9));
        green2.add(new Entry(26, (float) 14.1));
        green2.add(new Entry(27, (float) 14.3));
        green2.add(new Entry(28, (float) 14.5));
        green2.add(new Entry(29, (float) 14.8));
        green2.add(new Entry(30, (float) 15));
        green2.add(new Entry(31, (float) 15.2));
        green2.add(new Entry(32, (float) 15.4));
        green2.add(new Entry(33, (float) 15.6));
        green2.add(new Entry(34, (float) 15.7));
        green2.add(new Entry(35, (float) 16));
        green2.add(new Entry(36, (float) 16.2));
        green2.add(new Entry(37, (float) 16.4));
        green2.add(new Entry(38, (float) 16.6));
        green2.add(new Entry(39, (float) 16.7));
        green2.add(new Entry(40, (float) 17));
        green2.add(new Entry(41, (float) 17.2));
        green2.add(new Entry(42, (float) 17.4));
        green2.add(new Entry(43, (float) 17.6));
        green2.add(new Entry(44, (float) 17.8));
        green2.add(new Entry(45, (float) 18));
        green2.add(new Entry(46, (float) 18.2));
        green2.add(new Entry(47, (float) 18.4));
        green2.add(new Entry(48, (float) 18.6));
        green2.add(new Entry(49, (float) 18.9));
        green2.add(new Entry(50, (float) 19));
        green2.add(new Entry(51, (float) 19.2));
        green2.add(new Entry(52, (float) 19.4));
        green2.add(new Entry(53, (float) 19.6));
        green2.add(new Entry(54, (float) 19.9));
        green2.add(new Entry(55, (float) 20));
        green2.add(new Entry(56, (float) 20.2));
        green2.add(new Entry(57, (float) 20.4));
        green2.add(new Entry(58, (float) 20.6));
        green2.add(new Entry(59, (float) 20.9));
        green2.add(new Entry(60, (float) 21));

        green3.add(new Entry(24, (float) 15.3));
        green3.add(new Entry(25, (float) 15.5));
        green3.add(new Entry(26, (float) 15.8));
        green3.add(new Entry(27, (float) 16.1));
        green3.add(new Entry(28, (float) 16.3));
        green3.add(new Entry(29, (float) 16.6));
        green3.add(new Entry(30, (float) 16.9));
        green3.add(new Entry(31, (float) 17.1));
        green3.add(new Entry(32, (float) 17.4));
        green3.add(new Entry(33, (float) 17.6));
        green3.add(new Entry(34, (float) 17.8));
        green3.add(new Entry(35, (float) 18.1));
        green3.add(new Entry(36, (float) 18.35));
        green3.add(new Entry(37, (float) 18.5));
        green3.add(new Entry(38, (float) 18.75));
        green3.add(new Entry(39, (float) 19.1));
        green3.add(new Entry(40, (float) 19.35));
        green3.add(new Entry(41, (float) 19.5));
        green3.add(new Entry(42, (float) 19.65));
        green3.add(new Entry(43, (float) 20));
        green3.add(new Entry(44, (float) 20.25));
        green3.add(new Entry(45, (float) 20.5));
        green3.add(new Entry(46, (float) 20.65));
        green3.add(new Entry(47, (float) 20.85));
        green3.add(new Entry(48, (float) 21.25));
        green3.add(new Entry(49, (float) 21.45));
        green3.add(new Entry(50, (float) 21.65));
        green3.add(new Entry(51, (float) 21.85));
        green3.add(new Entry(52, (float) 22.25));
        green3.add(new Entry(53, (float) 22.45));
        green3.add(new Entry(54, (float) 22.65));
        green3.add(new Entry(55, (float) 22.85));
        green3.add(new Entry(56, (float) 23.25));
        green3.add(new Entry(57, (float) 23.45));
        green3.add(new Entry(58, (float) 23.65));
        green3.add(new Entry(59, (float) 23.85));
        green3.add(new Entry(60, (float) 24.1));


    }
}
