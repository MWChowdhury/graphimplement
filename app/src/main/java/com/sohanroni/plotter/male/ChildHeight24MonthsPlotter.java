package com.sohanroni.plotter.male;

import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rabby on 9/26/2018.
 */

public class ChildHeight24MonthsPlotter {
    LineChart chart;

    List<Entry> Orange1 = new ArrayList<Entry>();
    List<Entry> yollow2 = new ArrayList<Entry>();
    List<Entry> yollow3 = new ArrayList<Entry>();
    List<Entry> green1 = new ArrayList<Entry>();
    List<Entry> green2 = new ArrayList<Entry>();
    List<Entry> green3 = new ArrayList<Entry>();
    List<Entry> white1 = new ArrayList<Entry>();
    List<Entry> myentrie = new ArrayList<Entry>();
    LineDataSet mydataset;
    ArrayList<ILineDataSet> LstDataSet = new ArrayList<ILineDataSet>();

    public ChildHeight24MonthsPlotter() {
    }

    public void plotGraph(LineChart chart, List<Entry> entry) {
        this.chart = chart;
        this.myentrie = entry;
        DrawGraph();
    }


    private void DrawGraph() {

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            chart.setHardwareAccelerationEnabled(false);
        }

        chart.setKeepPositionOnRotation(true);
        chart.setAutoScaleMinMaxEnabled(true);
        chart.setPinchZoom(true);
        chart.setScaleEnabled(true);
        chart.fitScreen();

        XAxis xAxis = chart.getXAxis();
        xAxis.setLabelCount(24);
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.RED);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);

        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisRight().setDrawGridLines(false);

        Orange1.add(new Entry(0, 45));
        Orange1.add(new Entry(1, 49));
        Orange1.add(new Entry(2, (float) 53.5));
        Orange1.add(new Entry(3, 56));
        Orange1.add(new Entry(4, (float) 57.9));
        Orange1.add(new Entry(5, (float) 59.99));
        Orange1.add(new Entry(6, (float) 61.8));
        Orange1.add(new Entry(7, (float) 62.9));
        Orange1.add(new Entry(8, (float) 64.1));
        Orange1.add(new Entry(9, (float) 65.6));
        Orange1.add(new Entry(10, (float) 66.9));
        Orange1.add(new Entry(11, (float) 67.9));
        Orange1.add(new Entry(12, (float) 68.9));
        Orange1.add(new Entry(13, (float) 69.9));
        Orange1.add(new Entry(14, (float) 70.9));
        Orange1.add(new Entry(15, (float) 71.9));
        Orange1.add(new Entry(16, (float) 72.9));
        Orange1.add(new Entry(17, (float) 73.9));
        Orange1.add(new Entry(18, (float) 74.9));
        Orange1.add(new Entry(19, 75));
        Orange1.add(new Entry(20, (float) 75.9));
        Orange1.add(new Entry(21, (float) 76.5));
        Orange1.add(new Entry(22, (float) 77.6));
        Orange1.add(new Entry(23, (float) 78.1));
        Orange1.add(new Entry(24, (float) 78.9));


        yollow3.add(new Entry(0, (float) 48.2));
        yollow3.add(new Entry(1, (float) 52.5));
        yollow3.add(new Entry(2, (float) 56.9));
        yollow3.add(new Entry(3, (float) 59.8));
        yollow3.add(new Entry(4, (float) 61.8));
        yollow3.add(new Entry(5, (float) 63.8));
        yollow3.add(new Entry(6, (float) 65.9));
        yollow3.add(new Entry(7, (float) 67.1));
        yollow3.add(new Entry(8, (float) 68.9));
        yollow3.add(new Entry(9, (float) 69.9));
        yollow3.add(new Entry(10, (float) 71.1));
        yollow3.add(new Entry(11, (float) 72.8));
        yollow3.add(new Entry(12, (float) 73.8));
        yollow3.add(new Entry(13, (float) 74.9));
        yollow3.add(new Entry(14, (float) 75.9));
        yollow3.add(new Entry(15, (float) 76.9));
        yollow3.add(new Entry(16, (float) 77.9));
        yollow3.add(new Entry(17, (float) 78.9));
        yollow3.add(new Entry(18, (float) 79.9));
        yollow3.add(new Entry(19, (float) 80.9));
        yollow3.add(new Entry(20, (float) 81.9));
        yollow3.add(new Entry(21, (float) 82.9));
        yollow3.add(new Entry(22, (float) 83.7));
        yollow3.add(new Entry(23, (float) 84));
        yollow3.add(new Entry(24, (float) 84.9));


        green3.add(new Entry(0, 54));
        green3.add(new Entry(1, 59));
        green3.add(new Entry(2, 62));
        green3.add(new Entry(3, 66));
        green3.add(new Entry(4, 67));
        green3.add(new Entry(5, 70));
        green3.add(new Entry(6, 72));
        green3.add(new Entry(7, 74));
        green3.add(new Entry(8, 75));
        green3.add(new Entry(9, 77));
        green3.add(new Entry(10, 78));
        green3.add(new Entry(11, 79));
        green3.add(new Entry(12, 81));
        green3.add(new Entry(13, 82));
        green3.add(new Entry(14, 83));
        green3.add(new Entry(15, 85));
        green3.add(new Entry(16, 86));
        green3.add(new Entry(17, 87));
        green3.add(new Entry(18, 88));
        green3.add(new Entry(19, 89));
        green3.add(new Entry(20, 90));
        green3.add(new Entry(21, 91));
        green3.add(new Entry(22, 92));
        green3.add(new Entry(23, 93));
        green3.add(new Entry(24, 94));

        /*-----------------------------------------*/
        yollow2.add(new Entry(0, 47));
        yollow2.add(new Entry(1, 51));
        yollow2.add(new Entry(2, (float) 54.8));
        yollow2.add(new Entry(3, (float) 57.9));
        yollow2.add(new Entry(4, (float) 59.8));
        yollow2.add(new Entry(5, (float) 60.9));
        yollow2.add(new Entry(6, (float) 63.8));
        yollow2.add(new Entry(7, (float) 64.9));
        yollow2.add(new Entry(8, (float) 66.9));
        yollow2.add(new Entry(9, (float) 67.9));
        yollow2.add(new Entry(10, (float) 68.9));
        yollow2.add(new Entry(11, (float) 69.9));
        yollow2.add(new Entry(12, (float) 71.2));
        yollow2.add(new Entry(13, (float) 72.5));
        yollow2.add(new Entry(14, (float) 73.5));
        yollow2.add(new Entry(15, (float) 74.5));
        yollow2.add(new Entry(16, 75));
        yollow2.add(new Entry(17, (float) 76.1));
        yollow2.add(new Entry(18, 77));
        yollow2.add(new Entry(19, (float) 77.9));
        yollow2.add(new Entry(20, (float) 78.9));
        yollow2.add(new Entry(21, (float) 79.9));
        yollow2.add(new Entry(22, 81));
        yollow2.add(new Entry(23, (float) 81.2));
        yollow2.add(new Entry(24, 82));

        /*---------------------------------------------------*/
        green1.add(new Entry(0, 50));
        green1.add(new Entry(1, 55));
        green1.add(new Entry(2, 59));
        green1.add(new Entry(3, (float) 61.9));
        green1.add(new Entry(4, 64));
        green1.add(new Entry(5, 66));
        green1.add(new Entry(6, (float) 66.8));
        green1.add(new Entry(7, (float) 69.9));
        green1.add(new Entry(8, 71));
        green1.add(new Entry(9, (float) 72.1));
        green1.add(new Entry(10, (float) 73.8));
        green1.add(new Entry(11, (float) 74.9));
        green1.add(new Entry(12, 76));
        green1.add(new Entry(13, 77));
        green1.add(new Entry(14, (float) 78.2));
        green1.add(new Entry(15, (float) 79.9));
        green1.add(new Entry(16, (float) 80.5));
        green1.add(new Entry(17, (float) 81.5));
        green1.add(new Entry(18, (float) 82.9));
        green1.add(new Entry(19, (float) 83.9));
        green1.add(new Entry(20, (float) 84.9));
        green1.add(new Entry(21, (float) 85.7));
        green1.add(new Entry(22, (float) 86.1));
        green1.add(new Entry(23, 87));
        green1.add(new Entry(24, (float) 87.9));

        /*-----------------------------------------------------------------------*/

        green2.add(new Entry(0, 52));
        green2.add(new Entry(1, (float) 56.5));
        green2.add(new Entry(2, (float) 60.5));
        green2.add(new Entry(3, (float) 63.8));
        green2.add(new Entry(4, 66));
        green2.add(new Entry(5, (float) 68.1));
        green2.add(new Entry(6, (float) 69.9));
        green2.add(new Entry(7, (float) 71.8));
        green2.add(new Entry(8, (float) 72.5));
        green2.add(new Entry(9, (float) 74.5));
        green2.add(new Entry(10, 76));
        green2.add(new Entry(11, 77));
        green2.add(new Entry(12, (float) 78.5));
        green2.add(new Entry(13, (float) 79.5));
        green2.add(new Entry(14, (float) 80.8));
        green2.add(new Entry(15, (float) 81.8));
        green2.add(new Entry(16, (float) 82.8));
        green2.add(new Entry(17, (float) 83.9));
        green2.add(new Entry(18, 85));
        green2.add(new Entry(19, 86));
        green2.add(new Entry(20, 87));
        green2.add(new Entry(21, 88));
        green2.add(new Entry(22, 89));
        green2.add(new Entry(23, (float) 89.9));
        green2.add(new Entry(24, 91));

        /*-----------------------------------------------*/
        white1.add(new Entry(0, 55));
        white1.add(new Entry(1, (float) 60.1));
        white1.add(new Entry(2, (float) 65.1));
        white1.add(new Entry(3, (float) 67.1));
        white1.add(new Entry(4, 70));
        white1.add(new Entry(5, (float) 71.1));
        white1.add(new Entry(6, 74));
        white1.add(new Entry(7, (float) 75.1));
        white1.add(new Entry(8, (float) 77.1));
        white1.add(new Entry(9, (float) 78.1));
        white1.add(new Entry(10, (float) 80.1));
        white1.add(new Entry(11, (float) 81.1));
        white1.add(new Entry(12, (float) 82.3));
        white1.add(new Entry(13, 84));
        white1.add(new Entry(14, (float) 85.2));
        white1.add(new Entry(15, (float) 86.1));
        white1.add(new Entry(16, 88));
        white1.add(new Entry(17, 89));
        white1.add(new Entry(18, (float) 90.1));
        white1.add(new Entry(19, (float) 91.1));
        white1.add(new Entry(20, (float) 92.1));
        white1.add(new Entry(21, (float) 93.1));
        white1.add(new Entry(22, (float) 94.2));
        white1.add(new Entry(23, (float) 95.1));
        white1.add(new Entry(24, 97));

        LineDataSet dataSetYellow2 = new LineDataSet(yollow2, "yollow"); // add Orange1 to dataSetOrange1
        dataSetYellow2.setDrawFilled(true);
        dataSetYellow2.setCircleRadius(0);
        dataSetYellow2.setDrawCircles(false);
        dataSetYellow2.setFillAlpha(255);
        dataSetYellow2.setFillAlpha(255);
        //Drawable drawablea = ContextCompat.getDrawable(mContext, R.//Drawable.filldrawdeep);
        dataSetYellow2.setFillColor(Color.parseColor("#EBCF00"));
        dataSetYellow2.setValueTextColor(Color.GREEN);
        dataSetYellow2.setDrawValues(false);
        dataSetYellow2.setLineWidth((float) 0.5);
        dataSetYellow2.setColor(Color.BLACK);


        LineDataSet dataSetgreen1 = new LineDataSet(green1, "green"); // add Orange1 to dataSetOrange1
        dataSetgreen1.setDrawFilled(true);
        dataSetgreen1.setCircleRadius(0);
        dataSetgreen1.setDrawCircles(false);
        dataSetgreen1.setFillAlpha(255);
        //Drawable drawablesb = ContextCompat.getDrawable(mContext, R.//Drawable.filldrawm);
        dataSetgreen1.setFillColor(Color.parseColor("#00C00C"));
        dataSetgreen1.setValueTextColor(Color.GREEN);
        dataSetgreen1.setDrawValues(false);
        dataSetgreen1.setLineWidth((float) 0.5);
        dataSetgreen1.setColor(Color.BLACK);


        LineDataSet dataSetgreen2 = new LineDataSet(green2, "greeen"); // add Orange1 to dataSetOrange1
        dataSetgreen2.setDrawFilled(true);
        dataSetgreen2.setCircleRadius(0);
        dataSetgreen2.setDrawCircles(false);
        dataSetgreen2.setFillAlpha(255);
        dataSetgreen2.setFillColor(Color.parseColor("#00C00C"));
        dataSetgreen2.setValueTextColor(Color.GREEN);
        dataSetgreen2.setDrawValues(false);
        dataSetgreen2.setLineWidth((float) 0.5);
        dataSetgreen2.setColor(Color.BLACK);


        LineDataSet dataSetwhite1 = new LineDataSet(white1, "white"); // add Orange1 to dataSetOrange1
        dataSetwhite1.setDrawFilled(false);
        dataSetwhite1.setCircleRadius(0);
        dataSetwhite1.setDrawCircles(false);
        dataSetwhite1.setFillAlpha(255);
        dataSetwhite1.setValueTextColor(Color.GREEN);
        dataSetwhite1.setDrawValues(false);
        dataSetwhite1.setLineWidth((float) 0.5);
        dataSetwhite1.setColor(Color.BLACK);


        LineDataSet dataSetOrange1 = new LineDataSet(Orange1, "orange"); // add Orange1 to dataSetOrange1
        dataSetOrange1.setDrawFilled(true);
        dataSetOrange1.setDrawCircles(false);
        dataSetOrange1.setCircleRadius(0);
        dataSetOrange1.setFillAlpha(255);
        dataSetOrange1.setDrawValues(false);
        dataSetOrange1.setFillColor(Color.parseColor("#ff8300"));
        dataSetOrange1.setValueTextColor(Color.RED);
        dataSetOrange1.setLineWidth((float) 0.5);
        dataSetOrange1.setColor(Color.BLACK);

        LineDataSet dataSetyollow3 = new LineDataSet(yollow3, "yellow"); // add Orange1 to dataSetOrange1
        dataSetyollow3.setDrawFilled(true);
        dataSetyollow3.setCircleRadius(0);
        dataSetyollow3.setDrawCircles(false);
        dataSetyollow3.setFillAlpha(255);
        //dataSetyollow3.setFillDrawable(drawables);
        dataSetyollow3.setFillColor(Color.parseColor("#EDF026"));
        dataSetyollow3.setValueTextColor(Color.GREEN);
        dataSetyollow3.setDrawValues(false);
        dataSetyollow3.setLineWidth((float) 0.5);
        dataSetyollow3.setColor(Color.BLACK);


        LineDataSet dataSetgreen3 = new LineDataSet(green3, "point"); // add Orange1 to dataSetOrange1
        dataSetgreen3.setDrawFilled(true);
        dataSetgreen3.setCircleRadius(0);
        dataSetgreen3.setFillColor(Color.parseColor("#00C00C"));
        dataSetgreen3.setDrawCircles(false);
        dataSetgreen3.setCircleColor(Color.RED);
        dataSetgreen3.setDrawValues(false);
        dataSetgreen3.setValueTextColor(Color.BLACK);
        dataSetgreen3.setLineWidth((float) 0.5);
        dataSetgreen3.setColor(Color.BLACK);


        LstDataSet.add(dataSetwhite1);
        LstDataSet.add(dataSetgreen3);
        LstDataSet.add(dataSetgreen2);
        LstDataSet.add(dataSetgreen1);
        LstDataSet.add(dataSetyollow3);
        LstDataSet.add(dataSetYellow2);
        LstDataSet.add(dataSetOrange1);


        if (myentrie.size() > 0) {
            Log.v("rabby", "list Size:" + myentrie.size());
            mydataset = new LineDataSet(myentrie, "age");
            // add Orange1 to dataSetOrange1
            mydataset.setLineWidth(3);
            mydataset.setColor(Color.WHITE);
            mydataset.setDrawFilled(false);
            mydataset.setDrawCircles(true);
            mydataset.setCircleRadius(2);
            mydataset.setCircleColor(Color.BLUE);
            mydataset.setDrawValues(false);
            LstDataSet.add(mydataset);
        }

        LineData data = new LineData(LstDataSet);
        chart.setData(data);
        chart.getData().setHighlightEnabled(false);
        chart.invalidate();
    }
}
